## Commerce Time Slots

This is basically a commerce solution that gives the possibility to set a
particular time frame for an order to be picked up or delivered.

### Features
- Fully customizable time slots by providing a configurable set of days and the
given slot capacities.
- Configure 1 or more time slot entities for different purposes and easily
switch between them on the client's needs.
- Configurable maximum amount of days to display for the date picker. Also, can
configure the date when the end-user can start boking a time frame. By default,
it's from today.
- Configure the checkout flow visibility.
- Attach the desired timeslot to the order information in the "Other" section.
- A booking page where the shop admin can view the booked time slots and the
corresponding status (active/processed).

### Description
A time slot is the main configurator entity that consists of a certain amount of
days. You can't add a time slot without even 1 day.

A time slot day consists of a certain amount of day capacities. You can't add
a time slot day without event 1 capacity. The day can be normal or desired. A
normal day is a regular repeating one but the desired one is usually a holiday or
a specific date when there are some restrictions or certain unusual time frames.

The desired day works the way that it overrides a normal day configuration and if
there isn't a regular day aside, it won't affect the time slot.

A time slot day capacity is a specific time frame, for example: from 11:00
til 13:00. It also has a capacity, so, this shows the specific amount of orders
that may be booked in this particular time frame.

### Dependencies
- `https://www.drupal.org/project/commerce`
- `https://www.drupal.org/project/commerce_shipping`
- `https://www.drupal.org/project/jquery_ui_datepicker`

### Configuration pages
- Time slots: `admin/commerce/timeslots`
- Time slot days: `admin/commerce/timeslots/days`
- Time slot day capacities: `admin/commerce/timeslots/day-capacities`
- Time slot bookings: `admin/commerce/timeslots/booking`
- Time slots - Settings: `admin/commerce/config/timeslots`
